FROM oven/bun:1.0.33

WORKDIR /app
COPY package*.json ./
RUN bun i 
RUN bun i -g nodemon
COPY . .
EXPOSE 3000
CMD ["nodemon --exec 'bun' run dev"]


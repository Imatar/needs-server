import { TypedResponse } from "hono";

type Response<T = any> = TypedResponse<IResSuscess<T> | IResError>;

type IRes = {
  message: string;
};

type IResSuscess<T = any> = IRes & {
  data: T;
};

type IResError = IRes & {
  error: string;
};

export type { Response, IResError, IResSuscess };

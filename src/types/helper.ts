import { dirs } from "@/utils";

type IOption = {
  label: string;
  value: string;
};

type IDir = (typeof dirs)[number];

export type { IOption, IDir };

import { getFilePath } from "@/helper/path.helper";
import { Context, Next } from "hono";

const uploader = (fileName: string) => async (c: Context, next: Next) => {
  const body = await c.req.parseBody();

  const thumbnail = body[fileName];

  if (typeof thumbnail === "string" || Array.isArray(thumbnail)) {
    return c.json({
      error: "Thumbnail must be a file",
      message: "Thumbnail must be a file",
    });
  }
  const type = thumbnail.type;
  const name = thumbnail.name.toString();

  const split = name.split(".");

  const endName = split[0] + Date.now() + "." + type.split("/").at(-1);

  try {
    await writeFileFromFileTypeObject(thumbnail, getFilePath(endName));
  } catch (error) {
    return c.json({
      error,
      message: "Unable to upadload image try another one",
    });
  }

  c.req.upload = {
    isArray: false,
    fileName: endName,
  };
  await next();
};

async function writeFileFromFileTypeObject(file: File, filePath: string) {
  // create a blob from the file
  const fileBlob = new Blob([await file.arrayBuffer()]);

  // create a write stream to the file path
  const fileStreamWriter = Bun.file(filePath).writer();

  // read the file blob in chunks and write it to the file path
  const chunkSize = 1024 * 1024; // read the file blob in chunks of 1MB
  for (let start = 0; start < fileBlob.size; start += chunkSize) {
    const chunk = fileBlob.slice(start, start + chunkSize);
    const arrayBuffer = await chunk.arrayBuffer();
    const uint8Array = new Uint8Array(arrayBuffer);
    await fileStreamWriter.ready;
    fileStreamWriter.write(uint8Array);
  }

  // close the file stream writer
  fileStreamWriter.close();
}

export { uploader };

import { validator as honoValidator } from "hono/validator";
import { Schema, ValidationError } from "yup";

const validator = (schema: Schema) =>
  honoValidator("json", async (body, c) => {
    try {
      await schema.validate(body, {
        strict: true,
        abortEarly: true,
      });
      console.log("try");
    } catch (error) {
      console.log("catch");
      return c.json(
        {
          error: (error as ValidationError).errors?.[0],
          message: (error as ValidationError).errors?.[0],
        },
        400,
      );
    }
  });

export { validator };

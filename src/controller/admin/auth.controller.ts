import { Context } from "hono";
import * as bcrypt from "bcrypt";
import { Admin } from "@/models";
import { sign } from "hono/jwt";
import { envs } from "@/utils";

const saltRounds = 10;

const registerAdmin = async (c: Context) => {
  const body = await c.req.json();
  const password = await bcrypt.hash(body.password, saltRounds);

  const newUser = new Admin({
    ...body,
    password,
  });
  const savedUser = await newUser.save();

  return c.json({
    data: { ...savedUser },
    message: `Success`,
  });
};

const loginAdmin = async (c: Context) => {
  const { username, password } = await c.req.json();

  const foundAdmin = await Admin.findOne({ username }).select(
    "_id firstName username password isVerified role",
  );

  if (foundAdmin === null) {
    return c.json(
      {
        error: "Admin doesn't Exist",
        message: "Admin doesn't Exist, Check your username",
      },
      400,
    );
  }

  if (!foundAdmin.isVerified) {
    return c.json(
      {
        error: "Admin isn't verified ",
        message: "Admin isn't verified ",
      },
      400,
    );
  }

  const isPassOk = await bcrypt.compare(password, foundAdmin.password);

  if (!isPassOk) {
    return c.json(
      {
        error: "Incorrect Password",
        message: "Incorrect Password",
      },
      400,
    );
  }

  const adminTokenPayload = {
    role: foundAdmin.role,
    adminId: foundAdmin._id,
  };
  const adminToken = await sign(adminTokenPayload, envs.SECRET.ADMIN);

  return c.json({
    data: {
      token: adminToken,
      firstName: foundAdmin.firstName,
      role: foundAdmin.role,
    },
    message: `Success`,
  });
};

export { registerAdmin, loginAdmin };

import { IOption, Response } from "@/types";
import { Country } from "country-state-city";
import { Context } from "hono";

const getCountries = (c: Context): Response<IOption[]> => {
  const countries = Country.getAllCountries();
  const countriesLen = countries.length;
  const data: IOption[] = [];

  for (let i = 0; i < countriesLen; i++) {
    data.push({
      label: countries[i].name,
      value: countries[i].name,
    });
  }

  c.header("Cache-Control", "public, max-age=3600");
  return c.json({ data, message: "Countries" });
};

export { getCountries };

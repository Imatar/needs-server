import { Context } from "hono";
import { sign } from "hono/jwt";
import { OTP, envs } from "@/utils";
import { getRedisVal, setRedisVal } from "@/helper";
import { User } from "@/models";

const loginUser = async (c: Context) => {
  const body = await c.req.json();
  const email = body?.email;

  if (!email) {
    return c.json(
      {
        error: "email is a required field",
        message: "email is a required field",
      },
      400,
    );
  }

  await setRedisVal(email, OTP);

  const token = await sign({ email }, envs.SECRET.USER);

  return c.json({
    data: {
      token,
    },
    message: `OTP Sent on ${email}`,
  });
};

const registerUser = async (c: Context) => {
  const body = await c.req.json();
  const authToken = c.get("jwtPayload");

  if (!authToken?.email) {
    c.status(401);
    return c.json({
      error: "UnAuthorised",
      message: "UnAuthorised",
    });
  }

  const { email } = authToken;

  const foundUser = await User.findOne({ email })
    .select("_id firstName")
    .lean();

  if (foundUser?._id) {
    return c.json({ data: { newUser: true }, message: `OTP Sent on ${email}` });
  }

  const newUser = new User({ ...body, type: "user" });
  const savedUser = await newUser.save();

  const token = await sign(
    { email, userId: foundUser?._id, firstName: foundUser?.firstName },
    envs.SECRET.USER,
  );

  return c.json({
    data: { ...savedUser, token },
    message: `OTP Sent on ${email}`,
  });
};

const verifyUser = async (c: Context) => {
  const body = await c.req.json();

  const payload = c.get("jwtPayload");

  if (!payload?.email) {
    return c.json(
      {
        error: "OTP Expired",
        message: "OTP Expired",
      },
      400,
    );
  }

  const { email } = payload;

  if (!body?.otp) {
    return c.json(
      {
        error: "otp is a required field",
        message: "otp is a required field",
      },
      400,
    );
  }

  const otp = body.otp;
  const savedOtp = await getRedisVal(email);

  if (savedOtp !== otp) {
    return c.json(
      {
        email,
        error: "Incorrect OTP",
        message: "Incorrect OTP",
      },
      400,
    );
  }

  // correct OTP
  const foundUser = await User.findOne({ email })
    .select("_id firstName")
    .lean();

  if (foundUser === null) {
    const token = await sign({ email }, envs.SECRET.USER);

    return c.json({
      data: { newUser: true, token, firstName: foundUser?.firstName, email },
      message: "OTP Verified",
    });
  }

  const token = await sign({ email }, envs.SECRET.USER);

  return c.json({
    message: `OTP Verified`,
    data: { token, newUser: false, firstName: foundUser.firstName },
  });
};

export { registerUser, verifyUser, loginUser };

import { services } from "@/utils";
import { Context } from "hono";
import { uploadFile } from "@/helper";
import { Service } from "@/models";

const getAllServices = async (c: Context) => {
  c.header("Cache-Control", "public, max-age=7200");
  return c.json({
    data: services,
    message: "",
  });
};

const createService = async (c: Context) => {
  const body = await c.req.parseBody();
  const { title, desc } = body;

  const fileName = c.req.upload.fileName;
  uploadFile(fileName, "service");

  const newService = new Service({
    title,
    desc,
    thumbnail: fileName,
  });
  const savedService = await newService.save();

  return c.json({
    body: savedService,
    message: "",
  });
};

export { getAllServices, createService };

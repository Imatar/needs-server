import { services } from "@/utils";
import { Context } from "hono";

const getHomeServices = (c: Context) => {
  c.header("Cache-Control", "public, max-age=7200");
  return c.json({
    data: services,
    message: "",
  });
};

export { getHomeServices };

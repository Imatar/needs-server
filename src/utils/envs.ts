const env = process.env;

const isDev = env.MODE === "DEV";

const envs = {
  isDev,
  PORT: env.PORT || 3000,
  MONGO_URL: env.MONGO_URL || "",
  MINIO: {
    ENDPOINT: env.MINIO_ENDPOINT || "localhost",
    PORT: Number(env.MINIO_PORT) || 9000,
    USE_SSL: false,
    ACCESS_KEY: env.MINIO_ACCESS_KEY || "",
    SECRET_KEY: env.MINIO_SECRET_KEY || "",
    BUCKET: env.MINIO_BUCKET_NAME || "needs",
  },
  SECRET: {
    USER: "teSSerACT",
    ADMIN: "eSSerACT",
  },
  REDIS: {
    PORT: Number(env.REDIS_PORT ?? 6379),
    HOST: env.REDIS_HOST || "redis",
  },
};

export default envs;

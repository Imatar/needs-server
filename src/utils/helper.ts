type IMainService = {
  title: string;
  services: IService[];
};

type IService = {
  title: string;
  nodes: INode[];
};

type INode = {
  thumbnail: string;
  name: string;
};

const services: IMainService[] = [
  {
    title: "Salon Services",
    services: [
      {
        title: "Popular Services for Women",
        nodes: [
          {
            thumbnail: "/helper/women-1.png",
            name: "Hair Treatment",
          },
          {
            thumbnail: "/helper/women-2.png",
            name: "Haircut and Styling",
          },
          {
            thumbnail: "/helper/women-3.png",
            name: "Manicure / Pedicure",
          },
          {
            thumbnail: "/helper/women-4.png",
            name: "Hair Coloring & Facial",
          },
          {
            thumbnail: "/helper/women-5.png",
            name: "Hair Removal & Waxing",
          },
        ],
      },
      {
        title: "Popular Services for Men",
        nodes: [
          {
            thumbnail: "/helper/men-1.png",
            name: "Hair Treatment",
          },
          {
            thumbnail: "/helper/men-2.png",
            name: "Haircut and Styling",
          },
          {
            thumbnail: "/helper/men-3.png",
            name: "Manicure / Pedicure",
          },
          {
            thumbnail: "/helper/men-4.png",
            name: "Hair Coloring & Facial",
          },
          {
            thumbnail: "/helper/men-5.png",
            name: "Hair Removal & Waxing",
          },
        ],
      },
    ],
  },
  {
    title: "Services Listed 2",
    services: [
      {
        title: "Popular Services and Deals",
        nodes: [
          {
            thumbnail: "/helper/other-1.png",
            name: "Hair Treatment",
          },
          {
            thumbnail: "/helper/other-4.png",
            name: "Haircut and Styling",
          },
          {
            thumbnail: "/helper/other-5.png",
            name: "Manicure / Pedicure",
          },
          {
            thumbnail: "/helper/other-4.png",
            name: "Hair Coloring & Facial",
          },
          {
            thumbnail: "/helper/other-5.png",
            name: "Hair Removal & Waxing",
          },
        ],
      },
    ],
  },
];
export type { IMainService, IService, INode };
export { services };

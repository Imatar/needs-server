import { Hono } from "hono";
import { loginAdmin, registerAdmin } from "@/controller/admin/auth.controller";
import { registerAdminSchema, loginAdminSchema } from "@/schema";
import { validator } from "@/middleware";

const adminAuthRouter = new Hono();

adminAuthRouter.post(
  "/register",
  validator(registerAdminSchema),
  registerAdmin,
);

adminAuthRouter.post("/login", validator(loginAdminSchema), loginAdmin);

export default adminAuthRouter;

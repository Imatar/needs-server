import { Hono } from "hono";
import { getAllServices, createService } from "@/controller";
import { uploader } from "@/middleware";

const serviceRouter = new Hono();

serviceRouter.get("/", getAllServices);
serviceRouter.post("/", uploader("thumbnail"), createService);

export default serviceRouter;

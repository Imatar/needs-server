import { Hono } from "hono";

const healthRouter = new Hono();

healthRouter.get("/", (c) => {
  return c.json({ message: "Hello World!" });
});

export default healthRouter;

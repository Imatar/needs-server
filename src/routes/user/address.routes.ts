import { Hono } from "hono";
import { getCountries } from "@/controller";

const addressRouter = new Hono();

addressRouter.get("/countries", getCountries);

export default addressRouter;

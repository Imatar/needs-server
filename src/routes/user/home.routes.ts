import { Hono } from "hono";
import { getHomeServices } from "@/controller";

const homeRouter = new Hono();

homeRouter.get("/services", getHomeServices);

export default homeRouter;

import { Hono } from "hono";
import { validator } from "@/middleware";
import { jwt } from "hono/jwt";
import { registerUser, verifyUser, loginUser } from "@/controller";
import { envs } from "@/utils";
import { registerSchema } from "@/schema";

const authRouter = new Hono();

authRouter.post("/login", loginUser);

authRouter.use(jwt({ secret: envs.SECRET.USER }));

authRouter.post("/register", validator(registerSchema), registerUser);
authRouter.post("/verify", verifyUser);

export default authRouter;

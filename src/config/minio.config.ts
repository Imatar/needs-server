import { Client } from "minio";
import { envs } from "@/utils";

const { ENDPOINT, PORT, ACCESS_KEY, SECRET_KEY } = envs.MINIO;

var minioClient = new Client({
  endPoint: ENDPOINT,
  port: PORT,
  useSSL: false,
  accessKey: ACCESS_KEY,
  secretKey: SECRET_KEY,
});

export { minioClient };

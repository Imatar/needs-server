import { createClient } from "redis";
import { envs } from "@/utils";

const { REDIS } = envs;

var redisClient = createClient({
  socket: {
    port: REDIS.PORT,
    host: REDIS.HOST,
  },
});

const createConnection = async () => {
  redisClient
    .connect()
    .then(() => console.log("Redis Connected"))
    .catch(console.log);
};

const connectRedis = () => {
  if (!redisClient.isOpen) {
    createConnection();
  }
};

redisClient.on("error", function (err) {
  console.log("Error" + err);
  if (!redisClient.isOpen) {
    createConnection();
  }
});

export { connectRedis, redisClient };

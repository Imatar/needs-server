import { envs } from "@/utils";
import mongoose from "mongoose";

const connnectMongo = async () => {
  try {
    await mongoose.connect(envs.MONGO_URL);
    console.log("Mongodb Connected");
  } catch (error) {
    console.log(error);
    console.log("error connecting mongodb");
  }
};

export default connnectMongo;

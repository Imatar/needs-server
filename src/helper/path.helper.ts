import { IDir } from "@/types";
import { storage } from "@/utils";

const getObjectName = (fileName: string, dir: IDir) =>
  [dir, fileName].join("/");

const getFilePath = (fileName: string) => [storage, fileName].join("/");

export { getObjectName, getFilePath };

import { minioClient } from "@/config/minio.config";
import { IDir } from "@/types";
import * as fs from "node:fs/promises";
import { envs } from "@/utils";
import { getFilePath, getObjectName } from "./path.helper";

const bucketName = envs.MINIO.BUCKET;

const uploadFile = (fileName: string, dir: IDir) => {
  const filePath = getFilePath(fileName);
  const objectName = getObjectName(fileName, dir);

  minioClient.fPutObject(
    bucketName,
    objectName,
    filePath,
    {},
    async (err, objInfo) => {
      console.log(err, objInfo);
      if (err !== null) {
        console.log(`File ${objectName} cant be uploaded err:`, err);
      }
      await fs.unlink(filePath);
    },
  );
};

export { uploadFile };

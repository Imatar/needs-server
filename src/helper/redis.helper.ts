import { redisClient } from "@/config";

const setRedisVal = async (key: string, val: unknown, expire = 10 * 60) =>
  await redisClient.setEx(key, expire, JSON.stringify(val));

const getRedisVal = async (key: string, remove = false) => {
  var value = await redisClient.get(key);
  if (remove) {
    delRedisVal(key);
  }
  return value === null ? null : JSON.parse(value);
};

const delRedisVal = async (key: string) => await redisClient.del(key);

export { getRedisVal, setRedisVal, delRedisVal };

import { Hono } from "hono";
import { healthRouter, addressRouter, serviceRouter } from "@/routes";

const commonRouter = new Hono();

commonRouter.route("/health", healthRouter);
commonRouter.route("/address", addressRouter);
commonRouter.route("/services", serviceRouter);

export { commonRouter };

import { Hono } from "hono";
import { authRouter, homeRouter } from "@/routes";

const userRouter = new Hono();

userRouter.route("/auth", authRouter);
userRouter.route("/home", homeRouter);

export { userRouter };

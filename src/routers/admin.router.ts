import { Hono } from "hono";
import { adminAuthRouter } from "@/routes";

const adminRouter = new Hono();

adminRouter.route("/auth", adminAuthRouter);

export { adminRouter };

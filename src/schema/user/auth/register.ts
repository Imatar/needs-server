import { object, string, number } from "yup";

const registerSchema = object({
  firstName: string().required().min(3).max(255),
  lastName: string().required().min(3).max(255),
  email: string().email().required(),
  phone: number().required().integer().positive(),
  country: string().required(),
});

export default registerSchema;

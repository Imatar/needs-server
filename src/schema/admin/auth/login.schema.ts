import { object, string } from "yup";

const loginAdminSchema = object({
  username: string().min(4).max(25).required(),
  password: string().min(8).max(26).required(),
});

export default loginAdminSchema;

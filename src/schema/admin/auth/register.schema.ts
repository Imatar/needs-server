import { admins } from "@/utils";
import { object, string, boolean } from "yup";

const registerAdminSchema = object({
  firstName: string().required().min(3).max(255),
  lastName: string().required().min(3).max(255),
  username: string().min(4).max(25).required(),
  password: string().min(8).max(26).required(),
  isVerified: boolean().default(false),
  createdBy: string().required(),
  role: string().required().oneOf(admins),
});

export default registerAdminSchema;

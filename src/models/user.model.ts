import { model, Schema } from "mongoose";

type User = {
  firstName: string;
  lastName: string;
  email: string;
  phone: number;
  country: string;
};

const userSchema = new Schema<User>({
  firstName: {
    type: String,
    trim: true,
    minLength: 3,
    maxLength: 255,
    required: true,
  },
  lastName: {
    type: String,
    trim: true,
    minLength: 3,
    maxLength: 255,
    required: true,
  },
  email: {
    type: String,
    trim: true,
    lowercase: true,
    unique: true,
    required: true,
  },
  phone: {
    type: Number,
    required: true,
  },
  country: {
    type: String,
    required: true,
  },
});

const User = model("User", userSchema);
export default User;

import { Schema } from "mongoose";
import { genders, days, admins } from "@/utils";

type IGender = (typeof genders)[number];
type IDay = (typeof days)[number];

type IObjectId = typeof Schema.Types.ObjectId;

type IPlan = {
  serviceId: IObjectId;
  title: string;
  genders: IGender[];
  cutPrice: number;
  days: IDay[];
  price: number;
  timeFrom: number;
  timeTo: number;
  cancellationFee: number | null;
};

type IAdminRole = (typeof admins)[number];

type IAdmin = {
  firstName: string;
  lastName: string;
  isVerified: boolean;
  username: string;
  password: string;
  createdBy: IObjectId;
  role: IAdminRole;
};

type IService = {
  title: string;
  desc: string;
  thumbnail: string;
};

export type { IGender, IDay, IPlan, IAdmin, IService };

import { IService } from "./types";
import { Schema, Model, model } from "mongoose";

const serviceSchema = new Schema<IService, Model<IService>>(
  {
    title: {
      type: String,
      required: true,
    },
    desc: {
      type: String,
      required: true,
    },
    thumbnail: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  },
);

const Service = model("Service", serviceSchema);

export default Service;

import { model, Schema } from "mongoose";
import { genders, days } from "@/utils";
import { IPlan } from "./types";

const ObjectId = Schema.Types.ObjectId;

const planSchema = new Schema<IPlan>(
  {
    serviceId: {
      type: ObjectId,
      required: true,
    },
    title: {
      type: String,
      unique: true,
      trim: true,
      required: true,
    },
    genders: {
      type: [String],
      enum: genders,
      trim: true,
      required: true,
    },
    days: {
      type: [String],
      enum: days,
      trim: true,
      required: true,
    },
    cutPrice: {
      type: Number,
      required: true,
    },
    price: {
      type: Number,
      required: true,
    },
    timeFrom: {
      type: Number,
      required: true,
    },
    timeTo: {
      type: Number,
      required: true,
    },
    cancellationFee: {
      type: Number,
      required: true,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  },
);

const Plan = model("Plan", planSchema);
export default Plan;

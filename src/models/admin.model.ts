import { Schema, Model, model } from "mongoose";
import { IAdmin } from "./types";
import { admins } from "@/utils";

const adminSchema = new Schema<IAdmin, Model<IAdmin>>(
  {
    firstName: {
      type: String,
      trim: true,
      required: true,
    },
    lastName: {
      type: String,
      trim: true,
      required: true,
    },
    username: {
      type: String,
      trim: true,
      lowercase: true,
      unique: true,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
    isVerified: {
      type: Boolean,
      default: false,
      required: true,
    },
    createdBy: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    role: {
      type: String,
      enum: admins,
      required: true,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  },
);

const Admin = model("Admin", adminSchema);

export default Admin;

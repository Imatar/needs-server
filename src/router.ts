import { Hono } from "hono";
import { cors } from "hono/cors";
import { logger } from "hono/logger";
import { adminRouter, userRouter, commonRouter } from "@/routers";
import { connnectMongo, connectRedis } from "@/config";
import { Context } from "hono";

connnectMongo();
// connectRedis();

const app = new Hono();
app.use(cors());
app.use(logger());

app.get("/", (c: Context) => c.json({ ping: "pong" }));

app.route("/common", commonRouter);
app.route("/admin", adminRouter);
app.route("/user", userRouter);

export default app;
